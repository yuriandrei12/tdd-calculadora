package com.br.itau.calculadora.services;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public class Operacao {

    public static Integer soma(int numeroUm, int numeroDois){
        int resultado = numeroUm + numeroDois;
        return resultado;
    }

    public static Integer soma(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero : numeros) {
            resultado += numero;
        }
        return resultado;
    }

    public static Integer subtracao(int numeroUm, int numeroDois){
        int resultado = numeroUm - numeroDois;
        return resultado;
    }

    public static Integer subtracao(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero : numeros){
            if(resultado == 0){
                resultado = numero;
            } else{
                resultado -= numero;
            }
        }
        return resultado;
    }

    public static Integer multiplicacao(Integer numeroUm, Integer numeroDois){
        int resultado = numeroUm * numeroDois;
        return resultado;
    }

    public static Integer multiplicacao(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero : numeros){
            if (resultado == 0){
                resultado = numero;
            } else {
                resultado *= numero;
            }
        }
        return resultado;
    }

    public static Integer divisao(Integer numeroUm, Integer numeroDois){
        if (numeroUm == 0 || numeroDois == 0){
            throw new ArithmeticException("O valor 0 é invalido");
        } else {
            int resultado = numeroUm / numeroDois;
            return resultado;
        }
    }

    public static Integer divisao(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero : numeros){
            if (resultado == 0){
                resultado = numero;
            } else {
                resultado /= numero;
            }
        }
        return resultado;
    }
}