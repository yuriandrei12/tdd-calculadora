package com.br.itau.calculadora.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.omg.CORBA.INTERNAL;

import java.util.ArrayList;
import java.util.List;

public class OperacaoTests {

    @Test
    public void testarOperacaoSoma(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(Operacao.soma(numeros),6);
    }

    @Test
    public void testarOperacaoSomaDoisNumeros(){
        Assertions.assertEquals(Operacao.soma(1,2),3);
    }

    @Test
    public void testarOperacaoSubtracao(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(18);
        numeros.add(6);
        numeros.add(6);
        Assertions.assertEquals(Operacao.subtracao(numeros),6);
    }

    @Test
    public void testarOperacaoSubtracaoDoisNumeros(){
        Assertions.assertEquals(Operacao.subtracao(18,9),9);
    }

    @Test
    public void testarOperacaoMultiplicacao(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(3);
        numeros.add(4);
        Assertions.assertEquals(Operacao.multiplicacao(numeros),24);
    }

    @Test
    public void testarOperacaoMultiplicacaoDoisNumeros(){
        Assertions.assertEquals(Operacao.multiplicacao(3,3),9);
    }

    @Test
    public void testarOperacaoDivisao(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(20);
        numeros.add(2);
        numeros.add(5);
        Assertions.assertEquals(Operacao.divisao(numeros),2);
    }

    @Test
    public void testarOperacaoDivisaoDoisNumeros(){
        Assertions.assertEquals(Operacao.divisao(10,2),5);
    }

    @Test
    public void testarValorInvalidoDivisao(){
        Assertions.assertEquals(Operacao.divisao(0,1),"O valor 0 é invalido");
        Assertions.assertThrows(ArithmeticException.class, () -> {Operacao.divisao(0,1 );});
    }
}